// ==UserScript==
// @name       XML Serialization Script(XML Var Viewer)
// @namespace  http://www.rusticbit.com/
// @version    0.1
// @description  view a XML Variable as string
// @match      *://*/*
// @copyright  2012+, RusticBit
// ==/UserScript==

viewxml = function(xml){
    var oSerializer = new XMLSerializer();
	var sXML = oSerializer.serializeToString(xml);
    console.log(sXML);
}
