// ==UserScript==
// @name         Citibank Helper
// @namespace    http://www.rusticbit.com/
// @version      1.0
// @description  Disables Citibank's annoying login popup functionality and forces it to open a new tab
// @author       RusticBit
// @match        http://www.citibank.co.in/
// @grant        none
// ==/UserScript==

$( document ).ready(function() {
    //Wait till function 'onlineLogin1' is loaded
    waitForFunctionLoad();
});

function waitForFunctionLoad()
{
    if(onlineLogin1 == undefined){        
        window.setTimeout(waitForFunctionLoad,2000);
    }
    else{
        //Override function
        onlineLogin1 = function(){
            var mainwin;            
            mainwin=window.open("https://www.citibank.co.in/ibank/login/IQPin1.jsp");
        };
    }
}
