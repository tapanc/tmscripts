// ==UserScript==
// @name       IrctcHelper
// @namespace  http://rusticbit.com
// @version    0.1
// @description  Automates/Assists Tatkal ticket booking
// @match      *://www.irctc.co.in/eticketing/*
// @copyright  2014, RusticBit
// @require	   https://code.jquery.com/jquery-latest.js
// ==/UserScript==

$( document ).ready(function() {
    
    var url = document.location.toString()
    
    if(url == 'https://www.irctc.co.in/eticketing/trainbetweenstns.jsf'){
        
        window.setTimeout(function(){
            //Enter Passenger Details
            
            document.getElementById('addPassengerForm:psdetail:0:psgnName').value='Tapan Chandra';
            //document.getElementById('addPassengerForm:psdetail:1:psgnName').value='Vishnu Priya';
            
            document.getElementById('addPassengerForm:psdetail:0:psgnAge').value='28';
            //document.getElementById('addPassengerForm:psdetail:1:psgnAge').value='23';
            
            document.getElementById('addPassengerForm:psdetail:0:psgnGender').value='M';
            //document.getElementById('addPassengerForm:psdetail:1:psgnGender').value='F';
            
            if(document.getElementById('addPassengerForm:psdetail:0:idCardType')){
                document.getElementById('addPassengerForm:psdetail:0:idCardType').value = 'DRIVING_LICENSE';
                /document.getElementById('addPassengerForm:psdetail:1:idCardType').value = 'PANCARD';
            }
            
            if(document.getElementById('addPassengerForm:psdetail:0:idCardNumber')){
                document.getElementById('addPassengerForm:psdetail:0:idCardNumber').value = 'AP00920140002214';    
                document.getElementById('addPassengerForm:psdetail:1:idCardNumber').value = 'BXNPP6269H';
            }
            
            $('#j_captcha').blur(function() {
                //Go to next page
                $('input#validate').click();
            });
            
            document.getElementById('addPassengerForm:mobileNo').value = '8884000465';
            
            $('input#j_captcha').focus();
            
        },10000);
   }
                          
   		if(url == 'https://www.irctc.co.in/eticketing/jpInput.jsf?cid=1'){
       
            $('input[type="radio"][value="CREDIT_CARD"]').click();
            document.getElementById('jpBook:bankPGList').value= '30';
            document.getElementById('validate').click();
        }
        
        if(url == 'https://www.irctc.co.in/eticketing/journeySummary.jsf?cid=1'){
            
        }
        
    });