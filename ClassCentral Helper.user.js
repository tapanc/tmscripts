// ==UserScript==
// @name       ClassCentral Helper
// @namespace  http://www.rusticbit.com/
// @version    0.1
// @description  ClassCentral
// @match      https://www.class-central.com/user/courses
// @require http://code.jquery.com/jquery-latest.js
// @copyright  2014+, RusticBit
// ==/UserScript==

var interested_table = $('div#interested-table-wrapper table#interestedlist tbody#course-listing-tbody')[0];

annotate_rows(interested_table);

//sort_rows(interested_table);

function annotate_rows(tbl){
    var table_rows = tbl.children;
    
    for(var i=0;i < table_rows.length;i++){
        var start_date = table_rows[i].children[2];
        
        var startdate_attr = $(start_date).attr('itemprop');        
        
        if(start_date.innerText == 'Self paced')
        {
            $( start_date ).attr('sortorder','0');
        }
        else if (typeof startdate_attr !== typeof undefined && startdate_attr !== false) {
            var sort_order = (new Date($(start_date).attr('content')) - new Date('2014-01-01'))/(3600*24000);
            $( start_date ).attr('sortorder',sort_order);
        }
        
        console.log(start_date);
    }
}

function sort_rows(tbl){
    
	var table_rows = tbl.children;
    
    
    
    
    
}

function clearTable(tbl_body){
    var parent = document.getElementById(tbl_body);
    while(parent.hasChildNodes())
    {
        parent.removeChild(parent.firstChild);
    }
}